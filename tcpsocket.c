/***************************************************************************
 *   Copyright (C) 07/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#include "logging.h"
#include "sockaddr.h"

int tcp_listen(struct sockaddr_in *sa)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0 ) {
		log_print(LOG_ERROR, "tcp_listen_socket(): socket()");
		return -1;
	}

	int i = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i))) {
		log_print(LOG_ERROR, "tcp_listen_socket(): setsockopt(SO_REUSEADDR)");
		close(sock);
		return -1;
	}

	if (bind(sock, (struct sockaddr *)sa, sizeof(*sa))) {
		log_print(LOG_ERROR, "tcp_listen_socket(): bind(%s)", get_sockaddr_buf(sa));
		close(sock);
		return -1;
	}

	if (listen(sock, 8)) {
		log_print(LOG_ERROR, "tcp_listen_socket(): listen()");
		close(sock);
		return -1;
	}
	return sock;
}

int tcp_connect(struct sockaddr_in *sa)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0 ) {
		log_print(LOG_ERROR, "tcp_connect_socket(): socket()");
		return -1;
	}

	int ret = connect(sock, (struct sockaddr *)sa, sizeof(*sa));
	if (ret != 0) {
		log_print(LOG_ERROR, "tcp_connect(): connect(%s)", get_sockaddr_buf(sa));
		close(sock);
		return -1;
	}

	return sock;
}

int tcp_connect_nonblock(struct sockaddr_in *sa)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0 ) {
		log_print(LOG_ERROR, "tcp_connect_nonblock(): socket()");
		return -1;
	}

	int flags = fcntl(sock, F_GETFL, 0);
	if (flags < 0) {
		log_print(LOG_ERROR, "tcp_connect_nonblock(): fcntl(F_GETFL)");
		close(sock);
		return -1;
	}

	/* non-blocking connect() */
	if (fcntl(sock, F_SETFL, flags | O_NONBLOCK)) {
		log_print(LOG_ERROR, "tcp_connect_nonblock(): fcntl(F_SETFL)");
		close(sock);
		return -1;
	}

	int ret = connect(sock, (struct sockaddr *)sa, sizeof(*sa));
	if (ret && errno != EINPROGRESS) {
		log_print(LOG_ERROR, "tcp_connect_nonblock(): connect(%s)", get_sockaddr_buf(sa));
		close(sock);
		return -1;
	}

	/* reset EINPROGRESS */
	errno = 0;

	/* all further actions are blocking */
	if (fcntl(sock, F_SETFL, flags)) {
		log_print(LOG_ERROR, "tcp_connect_nonblock(): fcntl(F_SETFL)");
		close(sock);
		return -1;
	}

	return sock;
}

int tcp_connect_error(int fd)
{
	int err;
	unsigned int err_size = sizeof(err);
	if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &err_size)) {
		log_print(LOG_ERROR, "tcp_connect_error(): getsockopt(SO_ERROR)");
		return -1;
	}

	if (err) {
		errno = err;
		return -1;
	}

	return 0;
}
