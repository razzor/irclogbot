#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ircsession.h"
#include "logging.h"
#include "sockaddr.h"

int syslog_listen(const char *addr_str)
{
	struct sockaddr_in addr;
	parse_sockaddr(addr_str, &addr);

	int sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		log_print(LOG_ERROR, "socket()");
		return -1;
	}

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		log_print(LOG_ERROR, "bind()");
		return -1;
	}

	return sock;
}

int syslog_cb(int fd, void *privdata)
{
	struct irc_session *session = (struct irc_session *)privdata;

	char buf[1500];
	struct sockaddr_in addr;
	unsigned int i = sizeof(addr);

	int len = recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&addr, &i);
	if (len <= 0)
		return -1;

	if (session->state != IRC_JOINED)
		return 0;

	buf[len] = '\0';

	char *tmp = strchr(buf, ' ');
	tmp = strchr(tmp +1, ' ');
	tmp = strchr(tmp +1, ' ');

	irc_send(session, "PRIVMSG %s :%s: %s", session->channel, inet_ntoa(addr.sin_addr), tmp +1);
	return 0;
}
