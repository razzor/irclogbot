CFLAGS := -O2 -pipe -Wall

OBJS := configfile.o event.o ircsession.o linebuffer.o logging.o sockaddr.o syslog.o tcpsocket.o

all: irclogbot

irclogbot: $(OBJS) irclogbot.o
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.d: %.c
	$(CC) $(CFLAGS) -MM -c $< -o $@

clean:
	rm -f irclogbot *.d *.o *.log

DEPS := $(wildcard *.c)
-include $(DEPS:.c=.d)
