#ifndef _LINEBUFFER_H_
#define _LINEBUFFER_H_

#include <stdarg.h>

struct linebuffer {
	unsigned int size;
	unsigned int pos;

	char *newline;
	char *data;
};

struct linebuffer * create_linebuffer(int size);
void linebuffer_free(struct linebuffer *buf);

int linebuffer_clear(struct linebuffer *buf);

int linebuffer_readfd(struct linebuffer *buf, int fd);
int linebuffer_parsefd(struct linebuffer *buf, int fd);
int linebuffer_writefd(struct linebuffer *buf, int fd);

int linebuffer_put(struct linebuffer *buf, const char *src, unsigned int size);
int linebuffer_vprintf(struct linebuffer *buf, const char *fmt, va_list ap);
int linebuffer_printf(struct linebuffer *buf, const char *fmt, ...);

char * linebuffer_getline(struct linebuffer *buf, int *len);
int linebuffer_freeline(struct linebuffer *buf);

#endif /* _LINEBUFFER_H_ */
