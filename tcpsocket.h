#ifndef _TCPSOCKET_H_
#define _TCPSOCKET_H_

#include <netinet/ip.h>

int tcp_listen(struct sockaddr_in *sa);
int tcp_connect(struct sockaddr_in *sa);

int tcp_connect_nonblock(struct sockaddr_in *sa);
int tcp_connect_error(int fd);

#endif // _TCPSOCKET_H_
