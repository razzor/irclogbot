#ifndef _SYSLOG_H_
#define _SYSLOG_H_

int syslog_listen(const char *addr_str);
int syslog_cb(int fd, void *privdata);

#endif /* _SYSLOG_H_ */
