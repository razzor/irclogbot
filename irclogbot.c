#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "configfile.h"
#include "event.h"
#include "ircsession.h"
#include "logging.h"
#include "sockaddr.h"
#include "syslog.h"

#define DEFAULT_CONFIG "irclogbot.conf"

int main(int argc, char *argv[])
{
	/* parse config file */
	if (config_parse(DEFAULT_CONFIG))
		exit(1);

	char *syslog_addr = (char *)config_get_string("global", "syslog-addr", "0.0.0.0:514");
	int logsock = syslog_listen(syslog_addr);
	if (logsock == -1)
		exit(1);

	char *server_str = (char *)config_get_string("global", "server", NULL);
	if (server_str == NULL)
		exit(1);

	struct irc_session *session = irc_create_session();
	parse_sockaddr(server_str, &session->srv_addr);

	session->server_pass = (char *)config_get_string("global", "server-pass", NULL);
	session->nickname = (char *)config_get_string("global", "nickname", "logbot");
	session->username = (char *)config_get_string("global", "username", NULL);
	session->realname = (char *)config_get_string("global", "realname", NULL);

	session->channel = (char *)config_get_string("global", "channel", "logbot");
	session->channel_key = (char *)config_get_string("global", "channel-key", NULL);

	irc_connect(session);

	event_add_readfd(NULL, logsock, syslog_cb, session);
	event_loop();

	close(logsock);
	config_free();
	return 0;
}
