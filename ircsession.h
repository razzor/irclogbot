#ifndef _IRCSESSION_H_
#define _IRCSESSION_H_

#include <netinet/in.h>

struct irc_session {
	struct sockaddr_in srv_addr;
	struct event_fd *handler;
	int sock;

	int state;
	struct linebuffer *inbuf;
	struct linebuffer *outbuf;

	char *server_pass;
	char *nickname;
	char *username;
	char *realname;

	char *channel;
	char *channel_key;
};

enum {
	IRC_NONE = 0,
	IRC_CONNECTING,
	IRC_CONNECTION_FAILED,
	IRC_CONNECTED,
	IRC_MOTD_RECEIVED,
	IRC_JOINED,
	IRC_DISCONNECTED,
};

struct irc_session * irc_create_session(void);
void irc_destroy_session(struct irc_session *session);
int irc_connect(struct irc_session *session);

int irc_send(struct irc_session *session, const char *fmt, ...);

#endif /* _IRCSESSION_H_ */
